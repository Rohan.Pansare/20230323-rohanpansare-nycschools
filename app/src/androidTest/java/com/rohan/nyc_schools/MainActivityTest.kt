package com.rohan.nyc_schools

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.rohan.nyc_schools.ui.theme.NYC_SchoolsTheme
import org.junit.Rule
import org.junit.Test

class ListScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun verify_if_all_texts_exists() {

        // Start the app
        composeTestRule.setContent {
            NYC_SchoolsTheme() {
                MyApp()
            }
        }

        // Wait for the app to load
        composeTestRule.waitForIdle()
        composeTestRule.onNodeWithText("Schools").assertExists()
        //composeTestRule.onNodeWithText("Women's Academy").assertExists()
    }
}