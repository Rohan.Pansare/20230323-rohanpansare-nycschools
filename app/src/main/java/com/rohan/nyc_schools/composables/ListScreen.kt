package com.rohan.nyc_schools.composables

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.rohan.nyc_schools.data.School
import com.rohan.nyc_schools.network.RetrofitAPI

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SchoolList(navController: NavHostController) {
    var schools by remember { mutableStateOf(emptyList<School>()) }
    LaunchedEffect(Unit) {
        schools = RetrofitAPI.fetchSchools()
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Schools") },
                backgroundColor = MaterialTheme.colors.primary
            )
        },
    ) {
        LazyColumn(
            contentPadding = PaddingValues(16.dp)
        ) {
            items(schools) { school ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                        .clickable { navController.navigate("schoolDetails/${school.dbn}") },
                    elevation = 4.dp
                ) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        Text(
                            text = school.school_name,
                            style = MaterialTheme.typography.h6,
                            modifier = Modifier.padding(bottom = 8.dp)
                        )
                        Text(text = "Address: ${school.location}")
                    }
                }
            }
        }
    }
}
