package com.rohan.nyc_schools.composables

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.rohan.nyc_schools.data.SATScores
import com.rohan.nyc_schools.network.RetrofitAPI

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SchoolSATDetails(dbn: String) {
    var satDetails by remember { mutableStateOf(emptyList<SATScores>()) }
    LaunchedEffect(Unit) {
        satDetails = RetrofitAPI.getSATScoresByDbn(dbn)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "School SAT Details") },
                backgroundColor = MaterialTheme.colors.primary
            )
        },
    ) {
        LazyColumn(
            contentPadding = PaddingValues(16.dp)
        ) {
            items(satDetails) { satDetail ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                        .clickable { /* TODO */ },
                    elevation = 4.dp
                ) {
                    Column(modifier = Modifier.padding(16.dp)) {
                        Text(
                            text = satDetail.school_name,
                            style = MaterialTheme.typography.h6,
                            modifier = Modifier.padding(bottom = 8.dp)
                        )
                        Text(text = "Average Math Score: ${satDetail.sat_math_avg_score}")
                        Text(text = "Average Reading Score: ${satDetail.sat_critical_reading_avg_score}")
                        Text(text = "Average Writing Score: ${satDetail.sat_writing_avg_score}")
                    }
                }
            }
        }
    }
}
