package com.rohan.nyc_schools.network

import com.rohan.nyc_schools.data.SATScores
import retrofit2.http.GET
import retrofit2.http.Query

interface SATApi {
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATScores(@Query("dbn") dbn: String): List<SATScores>
}
