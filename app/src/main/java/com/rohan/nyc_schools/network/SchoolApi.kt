package com.rohan.nyc_schools.network

import com.rohan.nyc_schools.data.School
import retrofit2.http.GET

interface SchoolApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): List<School>
}
