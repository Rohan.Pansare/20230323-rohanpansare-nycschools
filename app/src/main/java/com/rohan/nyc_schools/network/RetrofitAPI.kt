package com.rohan.nyc_schools.network

import com.rohan.nyc_schools.data.School
import com.rohan.nyc_schools.data.SATScores
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitAPI {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val api_schools: SchoolApi = retrofit.create(SchoolApi::class.java)
    private val api_sat_scores: SATApi = retrofit.create(SATApi::class.java)

    suspend fun fetchSchools(): List<School> {
        return api_schools.getSchools()
    }

    suspend fun getSATScoresByDbn(dbn: String): List<SATScores> {
        return api_sat_scores.getSATScores(dbn)
    }
}
