package com.rohan.nyc_schools.data

data class School(
    val dbn: String,
    val school_name: String,
    val location: String
)
