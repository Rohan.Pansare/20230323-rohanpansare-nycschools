package com.rohan.nyc_schools.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.rohan.nyc_schools.composables.SchoolList
import com.rohan.nyc_schools.composables.SchoolSATDetails


@Composable
fun NavGraph() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "schoolList") {
        composable("schoolList") {
            SchoolList(navController = navController)
        }
        composable(
            "schoolDetails/{dbn}",
            arguments = listOf(navArgument("dbn") { type = NavType.StringType })
        ) { backStackEntry ->
            val dbn = backStackEntry.arguments?.getString("dbn") ?: ""
            SchoolSATDetails(dbn = dbn)
        }
    }
}