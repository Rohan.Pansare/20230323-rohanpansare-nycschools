This is Android application that displays information about schools in New York City. The code defines several composable functions that are used to display different screens in the application.

The MainActivity class is the main entry point for the application. It sets the content view of the activity to a Surface composable that fills the entire screen and contains the MyApp composable.

The MyApp composable is a wrapper function for the NavGraph composable, which sets up the navigation graph for the application. The navigation graph consists of two destinations: the school list screen and the school SAT details screen.

The SchoolList composable function is used to display a list of schools in New York City. It retrieves the list of schools from a RetrofitAPI instance and uses a LazyColumn composable to display each school in a Card composable. The Card composable is clickable, and when clicked, it navigates to the school SAT details screen for that school.

The SchoolSATDetails composable function is used to display the SAT scores for a particular school. It takes a dbn parameter, which is the unique identifier for the school, and uses a RetrofitAPI instance to retrieve the SAT scores for that school. It then displays the SAT scores in a LazyColumn composable, similar to how the SchoolList composable displays schools.

The NavGraph composable function sets up the navigation graph for the application using the NavHost composable. It defines two destinations: the school list screen and the school SAT details screen. The school SAT details screen has an argument for the dbn parameter, which is passed to the SchoolSATDetails composable function.

Finally, there is a DefaultPreview composable function, which is used to preview the application in Android Studio's layout editor. It uses the NYC_SchoolsTheme composable as a wrapper function and calls the SchoolList composable function to display a list of schools.

![Alt text](Screenshots/Screenshot-2.png)

![Alt text](Screenshots/Screenshot-1.png)
